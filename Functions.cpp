#include "Functions.h"

#include <iostream>
#include <string>
using namespace std;

// - PART 1: Stub functions ---------------------------------
// These functions just have placeholder values to satisfy the return type of their functions.
// These placeholders are meant to be replaced once tests are in place.

//! Returns the area of a rectangle given the `width` and `length`
int GetArea(int width, int length)
{
    return  width * length; // Return the actual return result
}

//! Unit tests for the GetArea function
void Test_GetArea()
{
    // the 1st test 
    string testName = "Test_GetArea() - Test 1: ";
    
    int input_width = 5;
    int input_height = 3;
    int expected_result = 15;
    int actual_result = GetArea(input_width, input_height);

    

    string result;


    if (actual_result != expected_result) { result = "[FAIL]\t"; }
    else { result = "[PASS]\t"; }
     
    cout << result << testName << endl;
    cout << "* input_width:     " << input_width << endl;
    cout << "* input_height:    " << input_height << endl;
    cout << "* expectedResult:  " << expected_result << endl;
    cout << "* actualResult:    " << actual_result << endl;
    cout << endl << endl;


  // The second test area
testName = "Test_GetArea() - Test 2: ";

 input_width = 7;
 input_height = 11;
 expected_result = 77;
 actual_result = GetArea(input_width, input_height);
 result = "";

if (actual_result != expected_result) { result = "[FAIL]\t"; }
else { result = "[PASS]\t"; }

cout << result << testName << endl;
cout << "* input_width: " << input_width << endl;
cout << "* input_height: " << input_height << endl;
cout << "* expectedResult: " << expected_result << endl;
cout << "* actualResult: " << actual_result << endl;
cout << endl <<endl;
}
//! Returns the amount of characters in the `text` string
int GetStringLength(string text)
{
    return text.size(); // returns the amount of characters in the text
}

//! Unit tests for the GetStringLength function
void Test_GetStringLength()
{
    // This is the first test;

    string test = "1st test for the length of a string";
    string text = "Hello";
    int expectedLength = 5;
    int actualLength = GetStringLength(text);

    // checking if the expected length and actual length match
    string result;
    if (actualLength != expectedLength) { result = "[FAIL]\t"; }
    else { result = "[PASS]\t"; }

    // Displaying the test results
    cout << result << test << endl;
    cout << "* input_text: " << text<< endl;
    cout << "* expected length of the text: " << expectedLength << endl;
    cout << "* actual length of the text: " << actualLength << endl; 
    cout << endl << endl;

    // 2nd test for the length of a string
    test = "2nd test for the length of a string";
    text = "abc def";
    expectedLength = 7;
    actualLength = GetStringLength(text);

    // checking if it the expected length and actual length match
    if (actualLength != expectedLength) { result = "[FAIL]\t"; }
    else { result = "[PASS]\t"; }

    // Display the test results
    cout << result << test << endl;
    cout << "* input_text: " << text << endl;
    cout << "* expected length of the text: " << expectedLength << endl;
    cout << "* actual length of the text: " << actualLength << endl;
    cout << endl << endl;


}

// - PART 2: Broken functions ------------------------------
// These functions are "implemented" but have logic errors. They should be flagged as
// failing on your unit tests. After you get the failing tests, you can come back
// and fix the logic.

//! Calculates and returns the perimeter of a rectangle given the `width` and the `length`
int GetPerimeter(int width, int length)
{
    return 2*(length + width); // Returns the actual results
}

//! Unit tests for the GetPerimeter function
void Test_GetPerimeter()
{
    string name = "Test _GetPerimeter():  1";
    int input_length = 10;
    int  input_width = 12;
    int expected_result =( (2 * input_length) + (2 * input_width));
    int actual_result = GetPerimeter(input_length, input_width);
    
    string result;
    
    // checking if the tests are correct.
    if (actual_result != expected_result) { result = "[FAIL]\t"; }
    else { result = "[PASS]\t"; }

    cout << result << name << endl;
    cout << "* input_width: " << input_width << endl;
    cout << "* input_length: " << input_length << endl;
    cout << "* expectedResult: " << expected_result << endl;
    cout << "* actualResult: " << actual_result << endl;
    cout << endl << endl;


    


    // Second test case
    name = "Test _GetPerimeter():  2";
    input_length = 77;
    input_width = 55;
    expected_result = ((2 * input_width) + (2 * input_length));
    actual_result = GetPerimeter(input_length, input_width);

    result;

    // checking if the tests are correct.
    if (actual_result != expected_result) { result = "[FAIL]\t"; }
    else { result = "[PASS]\t"; }

    // Displaying the test results
    cout << result << name << endl;
    cout << "* input_width: " << input_width << endl;
    cout << "* input_length: " << input_length << endl;
    cout << "* expectedResult: " << expected_result << endl;
    cout << "* actualResult: " << actual_result << endl;
    cout << endl << endl;

    
}

//! Returns the updated balance after `amount` is taken out from the original `balance`
float Withdraw(float balance, float amount)
{
    balance -= amount;
    return  balance; // Returns the balance after amount is taken out
}

//! Unit tests for the Withdraw function
void Test_Withdraw()
{
    string test1 = "1st Test for withdraw function";
    float balanceInput = 200.50;
    float amountInput = 100.50;
    float expectedBalance = 100.00;   
    float remainingBalance = Withdraw(balanceInput, amountInput);
    
    string answer;
    if (remainingBalance== expectedBalance) {
        answer = "[PASS]\t";
    }
    else {
        answer = "[FAIL}\t";
    }
    cout << answer << test1 << endl;
    cout << "* Input balance: " << balanceInput << endl;
    cout << "* Input amount: " << amountInput << endl;
    cout << "* Expected balance: " << expectedBalance << endl;
    cout << "* Actual balance after withdraw: " << remainingBalance<< endl;
    cout << endl<<endl ;

    // Test 2 for withdraw amount
     string test2 = "2nd Test for withdraw function";
     balanceInput = 1000.500;
     amountInput = 500.00;
     expectedBalance= 500.500;
     remainingBalance = Withdraw(balanceInput, amountInput);

     answer;
    if (remainingBalance == expectedBalance) {
        answer = "[PASS]\t";
    }
    else {
        answer = "[FAIL]\t";
    }
    cout << answer << test2 << endl;
    cout << "* Input balance: " << balanceInput << endl;
    cout << "* Input amount: " << amountInput << endl;
    cout << "* Expected amount balance: " << expectedBalance << endl;
    cout << "* Actual balance after withdraw: " << remainingBalance << endl;
    cout << endl<<endl;
}
